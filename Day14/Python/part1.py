num = int(open("../input.txt").readline())

elfR = [0, 1]
sb = [3,7]

def printsb():
    for i in range(len(sb)):
        if i in elfR:
            if elfR.index(i) == 0:
               print("({})".format(sb[i]), end='')
            else:
               print("[{}]".format(sb[i]), end='')
        else:
            print(" {} ".format(sb[i]), end='')
    print()

def getSum():
    sum = 0
    for elf in elfR:
        sum += sb[elf]
    return sum

#printsb()

while len(sb) < num+10:
    s = str(getSum())
    for n in s:
        sb.append(int(n))
    for j in range(len(elfR)):
        elfR[j] += ((sb[elfR[j]]+1)%len(sb))
        while(elfR[j] >= len(sb)):
            elfR[j] -= len(sb)
    #printsb()

print("".join(map(str,sb[num:num+10])))
