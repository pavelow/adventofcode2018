num = int(open("../input.txt").readline())
num = list(map(int, str(num)))
print(num)

elfR = [0, 1]
sb = [3,7]

def printsb():
    for i in range(len(sb)):
        if i in elfR:
            if elfR.index(i) == 0:
               print("({})".format(sb[i]), end='')
            else:
               print("[{}]".format(sb[i]), end='')
        else:
            print(" {} ".format(sb[i]), end='')
    print()

def getSum():
    sum = 0
    for elf in elfR:
        sum += sb[elf]
    return sum

def CheckPattern():
    if num[0] in sb:
        test = [j for j, e in enumerate(sb) if e == num[0]]
        for t in test:
            if sb[t:t+len(num)] == num:
                print(sb[t:t+len(num)])
                return t
    return False


#printsb()
i = 0
while True:
    cflag = False
    i+=1
    s = str(getSum())
    for n in s:
        sb.append(int(n))
        if int(n) in num:
            cflag = True
    for j in range(len(elfR)):
        elfR[j] += ((sb[elfR[j]]+1)%len(sb))
        while(elfR[j] >= len(sb)):
            elfR[j] -= len(sb)
    if(i%1000000 == 0):
        print(i) 
        a = CheckPattern()
        if a:
            print(a)
            break


print()
