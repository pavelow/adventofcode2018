#include <stdio.h>
#include <stdlib.h>
#define strlen 27 //Assume string length is 27

int main(int argc, char** argv) {
  FILE *f = fopen("../input.txt", "r");
  char str[strlen];

  int CountThree = 0;
  int CountTwo = 0;


  while(fscanf(f, "%s", &str) != EOF) {
    int Count[2] = {0};
    for(int i = 0; i< strlen; i++) {

      int lettercount = 0;

      for(int j = 0; j< strlen; j++) {
        if(str[i] == str[j]) {
          lettercount++;
        }
      }

      if(lettercount == 2) {
        Count[0]++;
      } else if(lettercount == 3) {
        Count[1]++;
      }
    }

    CountTwo += Count[0] > 0;
    CountThree += Count[1] > 0;

  }

  printf("%d:%d, so checksum is %d\n", CountTwo, CountThree, CountThree * CountTwo);


  fclose(f);
  return(0);
}
