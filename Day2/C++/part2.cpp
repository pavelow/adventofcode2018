#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char** argv) {
  std::ifstream f ("../input.txt");

  std::string line;
  std::vector<std::string> boxes;


  while(std::getline(f,line)) {
    boxes.push_back(line);
  }

  while(!boxes.empty()) {
    std::string box = boxes.back();
    boxes.pop_back();
    for(int i = 0; i < boxes.size(); i++) {
      int diff = 0;
      for(int j = 0; j < box.size(); j++) {
        if(box[j] != boxes[i][j]) {
          diff++;
        }
      }
      if(diff == 1) {
        std::cout << "Diff=1:\n" << box << "\n" << boxes[i] << std::endl;
      }
    }
  }

  f.close();
  return(0);
}
