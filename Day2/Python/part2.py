#! /usr/bin/python3

boxes = []

with open("../input.txt") as f:
    for id in f:
        boxes.append(id)

    while len(boxes) > 0:
        idt = boxes.pop()
        for cmpid in boxes:
            diff = 0
            for i in range(len(id)):  # Assume same length
                if(idt[i] != cmpid[i]):
                    diff += 1
            if(diff == 1):
                print("{0}{1}".format(idt, cmpid))
