#! /usr/bin/python3
import collections

CountThree = 0
CountTwo = 0

with open("../input.txt") as f:
    for id in f:
        res = collections.Counter(collections.Counter(id).values())
        CountTwo += res[2] > 0
        CountThree += res[3] > 0

    print(CountTwo * CountThree)
