import numpy as np
import math

serial = int(open("../input.txt").readline())
print(serial)
#serial = 42

A = np.zeros((300,300))

def getPower(x,y):
	global serial
	RID = x + 10
	PStart = (RID * y + serial)*RID
	return int(str(PStart)[-3]) - 5

for x in range(1,301):
	for y in range(1,301):
		A[x-1][y-1] = getPower(x,y)

max = 0
maxC = [0,0]
maxi = 0
for i in range(1,301):
	end = 300-i
	for y in range(1,end):
		for x in range(1,end):
			sum = A[x:x+i, y:y+i].sum()

			if (sum > max):
				maxi = i
				max = sum
				maxC = [x+1,y+1]
		#print(i)
	print("{} {} s:{} c:{}".format(max, maxC, maxi, i))
