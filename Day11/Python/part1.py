import numpy as np

serial = int(open("../input.txt").readline())
print(serial)
#serial = 42

A = np.zeros((300,300))

def getPower(x,y):
	global serial
	RID = x + 10
	PStart = (RID * y + serial)*RID
	return int(str(PStart)[-3]) - 5

for x in range(1,301):
	for y in range(1,301):
		A[x-1][y-1] = getPower(x,y)

max = 0
maxC = (0,0)
for x in range(0,297):
	for y in range(1,297):
		sum = A[x:x+3, y:y+3].sum()
		if (sum > max):
			max = sum
			maxC = (x+1,y+1)

print("{} {}".format(max, maxC))
