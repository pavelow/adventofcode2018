data = list(map(int, open("../input.txt").readline().split(' ')))


def GetNode(j, data):
    sum = 0
    NChild = data[j]
    NMeta = data[j+1]
    if(NChild == 0):
        for D in range(NMeta):
            sum += data[j+2+D]
        return (j+NMeta, sum)
    else:
        skip = j
        CValues = []
        for C in range(NChild):
            skip, value = GetNode(skip+2, data)
            CValues.append(value)
        for D in range(NMeta):
            Meta = data[skip+D+2]-1
            if(Meta >= 0 and Meta < len(CValues)):
                sum += CValues[Meta]
        return skip+NMeta, sum


print(GetNode(0, data)[1])
