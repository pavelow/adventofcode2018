from copy import deepcopy
from itertools import cycle

#Assume it is square
dirs = {}
dirs['^'] = (-1, 0)
dirs['v'] = (1, 0)
dirs['>'] = (0, 1)
dirs['<'] = (0, -1)

repl = {}
repl['^'] = '|'
repl['v'] = '|'
repl['>'] = '-'
repl['<'] = '-'


dirs['>\\'] = 'v'
dirs['>/'] = '^'
dirs['>-'] = '>'
dirs['>+'] = '>'
dirs['</'] = 'v'
dirs['<\\'] = '^'
dirs['<-'] = '<'

dirs['v\\'] = '>'
dirs['v/'] = '<'
dirs['v|'] = 'v'
dirs['v+'] = 'v'
dirs['^/'] = '>'
dirs['^\\'] = '<'
dirs['^|'] = '^'

left = {}
left['<'] = 'v'
left['v'] = '>'
left['^'] = '<'
left['>'] = '^'

right= {}
right['<'] = '^'
right['v'] = '<'
right['^'] = '>'
right['>'] = 'v'

carts = {}

mapA = []

with open("../input.txt") as f:
    for i in f:
        mapA.append([])
        for c in i:
            mapA[-1].append(c)
mapB = deepcopy(mapA)
mapO = deepcopy(mapA)

#populate clean map
for y in range(len(mapO)):
    for x in range(len(mapO[y])):
        c = mapO[y][x]
        if c in "<>^v":
            mapO[y][x] = repl[c]
            carts[(y, x)] = 0

def iterate():
    global mapA
    global mapB
    global carts
    for y in range(len(mapA)):
        for x in range(len(mapA[y])):
            c = mapA[y][x]
            if(c in "<>^v"):
                # We have a cart that needs update
                next = mapB[y+dirs[c][0]][x+dirs[c][1]]
                carts[(y+dirs[c][0], x+dirs[c][1])] = carts[(y, x)]
                if next in "<>^v":
                    mapA[y+dirs[c][0]][x+dirs[c][1]] = 'X'
                    print("Crash at x:{} y:{}".format(x+dirs[c][1], y+dirs[c][0]))
                    return False
                    for i in map:
                        print(i[0]) # [y][x]
                elif next == '+':
                    t = carts[(y, x)]
                    if(t < 2):
                        carts[(y+dirs[c][0], x+dirs[c][1])] = t+ 1
                    else:
                        carts[(y+dirs[c][0], x+dirs[c][1])] = 0
                    if t == 0:
                        mapB[y+dirs[c][0]][x+dirs[c][1]] = left[c]
                    elif t == 2:
                        mapB[y+dirs[c][0]][x+dirs[c][1]] = right[c]
                    else:
                        mapB[y+dirs[c][0]][x+dirs[c][1]] = c
                else:
                    mapB[y+dirs[c][0]][x+dirs[c][1]] = dirs[c+next]
                mapB[y][x] = mapO[y][x]
    mapA = deepcopy(mapB)
    return True

while True:
    if(not iterate()):
        break
