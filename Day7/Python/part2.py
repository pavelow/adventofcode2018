#! /usr/bin/python3

from parse import *


NWorkers = 5

steps = {} #forward deps
spets = {} #reverse deps
after = set()
before = set()

done = []
doing = []

with open("../input.txt") as f:
    for line in f:
        res = parse("Step {} must be finished before step {} can begin.", line)
        if res[0] not in steps:
            steps[res[0]] = []
        steps[res[0]].append(res[1])
        after.add(res[1])
        before.add(res[0])

        if res[1] not in spets:
            spets[res[1]] = []
        spets[res[1]].append(res[0])

print(spets)
print(steps)

start = list(before - after)
end = list(after - before)[0] #Find last step

viable = start

def findNext():
    if(len(doing) == 0):
        next = sorted(start)[0]
        viable.remove(next)
        return next
    for i in range(len(done)):
        if done[-(i+1)] != end:
            possible = steps[done[-(i+1)]] #Stuff that branches from current
            for s in possible:
                if((s not in done) and (s not in doing) and (s not in viable) and set(spets[s]) <= set(done)): #to do s we need spets[s] to be in done already
                    viable.append(s)
            if(len(viable) != 0):
                break
    if len(viable) == 0:
        next = '.'
    else:
        next = sorted(viable)[0]
        viable.remove(next)
    return next

Workers = []*NWorkers
for i in range(NWorkers):
    Workers.append({'doing':'.', 'time':0}.copy())



it = 0
while True:
    print('\n{:>3d} - '.format(it), end='')
    for W in Workers:
        if(W['doing'] != '.'):
            W['time'] -= 1
        if(W['time'] <= 0):
            if(W['doing'] != '.'):
                done.append(W['doing'])
                doing.remove(W['doing'])
            W['doing'] = findNext()
            doing.append(W['doing'])
            if(W['doing'] != '.'):
                W['time'] = 60+ ord(W['doing']) - ord('A')+1
            else:
                W['time'] = 0
        print('{} '.format(W['doing']), end='')
    it += 1
    if len(done) > 0 and done[-1] == end:
        break
    print('-> ' + ''.join(done), end='')

    #print(Workers)
    #print('Seq:{}'.format(done))

print(''.join(done))
