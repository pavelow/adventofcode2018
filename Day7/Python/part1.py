#! /usr/bin/python3

from parse import *

steps = {} #forward deps
spets = {} #reverse deps
after = set()
before = set()

sequence = []

with open("../input.txt") as f:
    for line in f:
        res = parse("Step {} must be finished before step {} can begin.", line)
        if res[0] not in steps:
            steps[res[0]] = []
        steps[res[0]].append(res[1])
        after.add(res[1])
        before.add(res[0])

        if res[1] not in spets:
            spets[res[1]] = []
        spets[res[1]].append(res[0])

start = list(before - after)
sequence.append(sorted(start)[0]) #Find first step
start.remove(sorted(start)[0])
end = list(after - before)[0] #Find last step

viable = start
while sequence[-1] != end:
    for i in range(len(sequence)):
        #print(i)
        possible = steps[sequence[-(i+1)]] #Stuff that branches from current
        print("Pot:{}:{}".format(sequence[-(i+1)],possible))
        for s in possible:
            if(s not in sequence and set(spets[s]) <= set(sequence)): #to do s we need spets[s] to be in sequence already
                viable.append(s)
                print("Req:{}:{}".format(s, spets[s])) #Stuff required by current
        print("Via:{}".format(viable))
        if(len(viable) != 0):
            break


    sequence.append(sorted(viable)[0])
    print('Seq:{}'.format(sequence))
    viable.remove(sorted(viable)[0])

print(''.join(sequence))
