#! /usr/bin/python3
from parse import parse

currentGuard = -1
data = {}
sortedData = []

Sleep = {}
SleepTime = {}

with open("../input.txt") as f:
    for line in f:
        # [1518-06-12 00:00] Guard #3359 begins shift
        parsed = parse("[{}] {}", line)
        data[parsed[0]] = line

    for key in sorted(data.keys()):
        sortedData.append(data[key])

data = {}

for line in sortedData:
    # [1518-06-12 00:00] Guard #3359 begins shift
    parsed = parse("[{}-{}-{} {}:{}] {}\n", line)
    action = parsed[5].split(' ')
    if(action[0] == "Guard"):
        currentGuard = action[1][1:] + "-" + parsed[2] + "-" + parsed[1] + "-" + parsed[0]
        data[currentGuard] = [0]*60
    else:
        if currentGuard != -1 and parsed[3] == '00':
            sleeping = action[0] == "falls"
            for i in range(int(parsed[4]), 60):
                data[currentGuard][i] = sleeping

for key in data:
    value = data[key]
    guardid = key.split('-')[0]
    if (guardid not in SleepTime):
        Sleep[guardid] = [0]*60
        SleepTime[guardid] = 0
    for i in range(60):
        SleepTime[guardid] += value[i]
        Sleep[guardid][i] += value[i]

max = 0
maxID = -1
for key in SleepTime:
    if SleepTime[key] > max:
        max = SleepTime[key]
        maxID = key

max = 0
maxT = 0
for i in range(60):
    if Sleep[maxID][i] > max:
        max = Sleep[maxID][i]
        maxT = i

print("ID:{0} Minute:{1}, {2}".format(maxID, maxT, int(maxID)*maxT))
