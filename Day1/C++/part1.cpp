#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
  FILE *f = fopen("../input.txt", "r");

  int change;
  int total = 0;

  while(fscanf(f, "%d", &change) != EOF) {
    total += change;
  }
  printf("%d\n",total);
  fclose(f);

  return(0);
}
