#include <stdio.h>
#include <stdlib.h>
#include <set>


int main(int argc, char** argv) {
  FILE *f = fopen("../input.txt", "r");

  int change;
  int total = 0;
  std::set<int> numlist;

  for(;;) {
    if(fscanf(f, "%d", &change) != EOF) {
      total += change;
      if(numlist.find(total) != numlist.end()) {
        printf("Found Duplicate: %d\n",total);
        return(0);
      }
      numlist.insert(total);
    } else {
      fseek(f, 0, SEEK_SET);
    }


  }


  printf("%d\n",total);
  fclose(f);

  return(0);
}
