#! /usr/bin/python3
from itertools import cycle

numlist = set()
with open("../input.txt") as f:
    freq = cycle(list(map(int, f)))

num = 0
while True:
    for change in freq:
        if num in numlist:
            print("First Duplicate: {0}".format(num))
            exit()
        numlist.add(num)
        num += change
