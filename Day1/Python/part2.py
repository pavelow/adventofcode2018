#! /usr/bin/python3


def main():
    numlist = set()
    num = 0
    f = open("../input.txt")
    while True:
        for change in f:
            if num in numlist:
                print("First Duplicate: {0}".format(num))
                return

            numlist.add(num)
            num += int(change)
        f.seek(0, 0)  # Restart list
    f.close()


main()
