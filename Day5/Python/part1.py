#! /usr/bin/python3

polymer = open("../input.txt").readline()[:-1]


def react(polymer):
    out = ""
    for c in polymer:
        if len(out) == 0:
            out = c
        elif ((c.isupper() and out[-1].islower()) or (out[-1].isupper() and c.islower())) and (c.upper() == out[-1].upper()):
            out = out[:-1]
        else:
            out += c
    return out


outpolymer = react(polymer)

print(len(polymer))
print(len(outpolymer))
