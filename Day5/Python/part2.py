#! /usr/bin/python3

polymer = open("../input.txt").readline()[:-1]


def react(l, polymer):
    out = ""
    for c in polymer:
        if c.upper() == l.upper():
            pass
        elif len(out) == 0:
            out = c
        elif ((c.isupper() and out[-1].islower()) or (out[-1].isupper() and c.islower())) and (c.upper() == out[-1].upper()):
            out = out[:-1]
        else:
            out += c
    return out


ans = []
letters = ''.join(set(polymer.upper()))
print(letters)
for letter in letters:
    print(letter)
    ans.append(react(letter, polymer))

answer = min(ans, key=len)
print(len(answer))
