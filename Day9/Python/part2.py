from collections import deque
from itertools import cycle, chain

data = open("../input.txt").readline().split(' ')
data = list(map(int, [data[i] for i in range(len(data)) if i in [0, 6]]))

players = cycle([i+1 for i in range(data[0])])
playerscore = [0]*data[0]
marbles = chain([i+1 for i in range(data[1]*100)])

circle = deque()
circle.append(0)

for p in players:
    m = next(marbles, None)
    if(m != None):
        if(m % 23 == 0):
            circle.rotate(7)
            playerscore[p-1] += m + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(m)
    else:
        break

print("HighScore: {}".format(max(playerscore)))
