from parse import parse
import numpy as np

coords = []
Xmax = 0
Ymax = 0

with open("../input.txt") as f:
    for coord in f:
        c = parse("{}, {}\n", coord)
        coords.append((int(c[0]), int(c[1])))
        if(int(c[0]) > Xmax):
            Xmax = int(c[0])
        if(int(c[1]) > Ymax):
            Ymax = int(c[1])

DMap = np.full((Xmax+1, Ymax+1), 0)


def dist(c1, c2):
    X = abs(c1[0] - c2[0])
    Y = abs(c1[1] - c2[1])
    return (X+Y)


for idx, coord in enumerate(coords):
    for i in range(Ymax+1):
        for j in range(Xmax+1):
            DMap[j][i] += dist(coord, (j, i))

count = 0
for i in range(Ymax+1):
    for j in range(Xmax+1):
        if DMap[j][i] < 10000:
            count += 1

print(count)
