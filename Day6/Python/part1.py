from parse import parse
import numpy as np

coords = []
Xmax = 0
Ymax = 0

with open("../input.txt") as f:
    for coord in f:
        c = parse("{}, {}\n", coord)
        coords.append((int(c[0]), int(c[1])))
        if(int(c[0]) > Xmax):
            Xmax = int(c[0])
        if(int(c[1]) > Ymax):
            Ymax = int(c[1])

Map = np.full((Xmax+1, Ymax+1), -1)
DMap = np.full((Xmax+1, Ymax+1), 10000)


def dist(c1, c2):
    X = abs(c1[0] - c2[0])
    Y = abs(c1[1] - c2[1])
    return (X+Y)


for idx, coord in enumerate(coords):
    for i in range(Ymax+1):
        for j in range(Xmax+1):
            d = dist(coord, (j, i))
            if DMap[j][i] > d:
                DMap[j][i] = d
                Map[j][i] = idx+1
            elif DMap[j][i] == d:
                Map[j][i] = -2


def count(k):
    count = 0
    idx = k+1
    for i in range(Ymax+1):
        for j in range(Xmax+1):
            if Map[j][i] == idx:
                if i == 0 or j == 0 or i == Ymax or j == Xmax:
                    return 0
                else:
                    count += 1
    return count


countMax = 0
for k in range(len(coords)):
    c = count(k)
    if c > countMax:
        countMax = c

print(countMax)
