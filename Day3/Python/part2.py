#! /usr/bin/python3
import numpy as np

claimList = np.zeros((1000, 1000))


def main():
    with open("../input.txt") as f:
        for c in f:
            parts = c.split(' ')
            id = int(parts[0][1:])
            start = parts[2].split(',')
            start[1] = int(start[1][0:-1])
            start[0] = int(start[0])
            size = parts[3].split('x')
            size[0] = int(size[0])
            size[1] = int(size[1])

            for i in range(size[0]):
                for j in range(size[1]):
                    x = i+start[0]
                    y = j+start[1]
                    claimList[x][y] += 1

        f.seek(0, 0)  # Restart list

        for c in f:
            parts = c.split(' ')
            id = int(parts[0][1:])
            start = parts[2].split(',')
            start[0] = int(start[0])
            start[1] = int(start[1][0:-1])
            size = parts[3].split('x')
            size[0] = int(size[0])
            size[1] = int(size[1])
            overlapflag = False

            for i in range(size[0]):
                for j in range(size[1]):
                    x = i+start[0]
                    y = j+start[1]
                    if(claimList[x][y] > 1):
                        overlapflag = True
            if(not overlapflag):
                print("ID:{} is not overlapping".format(id))

    size = 0
    for i in range(1000):
        for j in range(1000):
            if(claimList[i][j] > 1):
                size += 1

    print("Total Overlap Size: {0}".format(size))


if __name__ == '__main__':
    main()
