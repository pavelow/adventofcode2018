#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  FILE *f = fopen("../input.txt", "r");

  int id;
  int size[2];
  int pos[2];

  int Claims[1000][1000] = {};

  while(fscanf(f, "#%d @ %d,%d: %dx%d\n", &id, &pos[0], &pos[1], &size[0], &size[1]) != EOF) {
    for(int i = 0; i < size[0]; i++) {
      for(int j = 0; j < size[1]; j++) {
        int x = i + pos[0];
        int y = j + pos[1];
        Claims[x][y] += 1;
      }
    }
  }

  fseek(f, 0, SEEK_SET);

  while(fscanf(f, "#%d @ %d,%d: %dx%d\n", &id, &pos[0], &pos[1], &size[0], &size[1]) != EOF) {
    bool overlapping = false;
    for(int i = 0; i < size[0]; i++) {
      for(int j = 0; j < size[1]; j++) {
        int x = i + pos[0];
        int y = j + pos[1];
        if(Claims[x][y] > 1) {
          overlapping = true;
        }
      }
    }
    if(!overlapping) {
      printf("ID:%d not overlapping\n", id);
    }
  }

  int AreaCount = 0;
  for(int i = 0; i < 1000; i++) {
    for(int j = 0; j < 1000; j++) {
      if(Claims[i][j] > 1) {
        AreaCount++;
      }
    }
  }

  printf("Area %d\n", AreaCount);



  fclose(f);
  return(0);
}
