from parse import parse
from collections import deque
import itertools

StateA = deque()
StateB = deque()

rules = {}

with open("../input.txt") as f:
	initial = parse("initial state: {}",f.readline())[0]
	for char in initial:
		StateA.append(char)
	f.readline() #
	for l in f:
		rules[l[0:5]] = l[9]


#print(rules)
leftoffset = 0
print(''.join(StateA))
for i in range(20):
	for j in range(len(StateA)):
		if j-2 == -2:
			state = ".."+''.join(list(map(str, itertools.islice(StateA, j, j+3))))
		elif j-2 == -1:
			state = "."+''.join(list(map(str, itertools.islice(StateA, j-1, j+3))))
		elif len(StateA)-j == 2:
			state = ''.join(list(map(str, itertools.islice(StateA, j-2, j+3))))+"."
		elif len(StateA)-j == 1:
			state = ''.join(list(map(str, itertools.islice(StateA, j-2, j+1))))+".."
		else:
			state = ''.join(list(map(str, itertools.islice(StateA, j-2, j+3))))
		#print(state)

		if state in rules:
			StateB.append(rules[state])
		else:
			StateB.append(".")

	#End part
	state = ("..." + ''.join(list(map(str, itertools.islice(StateA, 0, 2)))))
	if state in rules:
		if rules[state] != '.':
			leftoffset += 1
			StateB.appendleft(rules[state])

	state = (''.join(list(map(str, itertools.islice(StateA, len(StateA)-2, len(StateA)))))+"...")
	if state in rules:
		if rules[state] != '.':
			StateB.append(rules[state])

	while StateB[0] == '.':
		StateB.popleft()
		leftoffset -= 1


	#print(str(i) +''.join(StateB))
	StateA = StateB
	StateB = deque()
	if(i%100000 == 0):
		print("{0:.10f}".format(i/50000000000 * 100))

sum = 0
for i in range(len(StateA)):
	k = i - leftoffset
	if StateA[i] == '#':
		sum +=k

print(sum)
