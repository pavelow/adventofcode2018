from parse import parse


class point:
    def __init__(self, a):
        a = list(map(int, a))
        self.x = a[0]
        self.y = a[1]
        self.xV = a[2]
        self.yV = a[3]

    def incr(self, t):
        self.x += self.xV*t
        self.y += self.yV*t


points = []

for line in open("../input.txt"):
    points.append(point(parse("position=<{},{}> velocity=<{},{}>\n", line)))


def findArea(points):
    minX = points[0].x
    maxX = 0
    minY = points[0].y
    maxY = 0
    for p in points:
        if(p.x > maxX):
            maxX = p.x
        if(p.x < minX):
            minX = p.x
        if(p.y > maxY):
            maxY = p.y
        if(p.y < minY):
            minY = p.y

    A = abs(maxX - minX)*abs(maxY - minY)
    return (A, [maxX, minX, maxY, minY])


# Gradiant Descent
Dir = 1
min = findArea(points)[0]
lmin = 0
llmin = 0
total = 0
while True:
    A = findArea(points)[0]
    llmin = lmin
    lmin = min

    if A < min:
        min = A
    elif A > min:
        Dir = -Dir
    if abs(llmin - min) < 1:
        break
    for p in points:
        p.incr(Dir)
    total += Dir

# Print word
A = findArea(points)
X = abs(A[1][0]-A[1][1])
Y = abs(A[1][2]-A[1][3])

Word = Matrix = [[' ' for y in range(Y+1)] for x in range(X+1)]

for p in points:
    Word[p.x-A[1][1]][p.y-A[1][3]] = '#'

for y in range(Y+1):
    for x in range(X+1):
        print(Word[x][y], end='')
    print()

print("Total Time: {}".format(total))
